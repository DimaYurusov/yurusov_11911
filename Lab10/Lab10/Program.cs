﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная  №10");
            Console.Write("Введите номер задания ");


            var task = Console.ReadLine();

            switch (task)
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;
                case "3":
                    Task3();
                    break;
                case "4":
                    Task4();
                    break;
                case "5":
                    Task5();
                    break;
                case "6":
                    Task6();
                    break;
            }
        }
        static void Task1()
        {
            Console.WriteLine("Введит сообщение:");
            string sentens = Console.ReadLine();
            Console.WriteLine("Введите слово:");
            string pattern = Console.ReadLine();
            if (Regex.IsMatch(sentens, pattern)) 
            Console.WriteLine("Найденное слово: {0}", pattern);
            else
                Console.WriteLine("Слово не найдено");

        }
        static void Task2()
        {
            Console.WriteLine("Введит сообщение:");
            string sentens = Console.ReadLine();
            Console.WriteLine("Введите длину слова");
            string n = Console.ReadLine();
            string pattern = @"\b\w{" + n + @"}\b";
            Match match = Regex.Match(sentens, pattern);
            if (match.Success)
            {
                Console.WriteLine("Слова введенной длины  : {0}", match);
            }
            else
                Console.WriteLine("Слов введенной длины нет");

        }
        static void Task3()
        {
            Console.WriteLine("Введит сообщение:");
            string sentens = Console.ReadLine();
            string pattern = @"\b\p{Lu}+\w*\b";
            foreach (Match match in Regex.Matches(sentens, pattern))
                Console.WriteLine("Слова , начинающиеся с заглавной буквы  : {0}", match);
        }
        static void Task4()
        {
            Console.WriteLine("Введит сообщение:");
            string sentens = Console.ReadLine();
            string pattern = @"\p{P}";

            Console.WriteLine("Предложение без знаков препинания: {0} ", Regex.Replace(sentens, pattern, String.Empty));
        
        }
        static void Task5()
        {
            Console.WriteLine("Введит сообщение:");
            string sentens = Console.ReadLine();
            string pattern = @"\b[a-zA-z]+\b";
            string replacement = "...";
            Console.WriteLine("Предложение , без английских слов: {0} ", Regex.Replace(sentens, pattern, replacement));
        }
        static void Task6()
        {
            double sum = 0;
            Console.WriteLine("Введит сообщение:");
            string sentens = Console.ReadLine();
            string pattern = @"[+-]?\d+?\.?\d*\b";
            foreach (Match match in Regex.Matches(sentens, pattern))
              if (match.Success)
                {
                    sum += double.Parse(match.Value);
                    

                }
               
                Console.WriteLine("Сумма цифр в тексте  : {0}", sum);
        }
    }
}
