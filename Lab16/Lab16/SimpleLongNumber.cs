﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab16
{
    class SimpleLongNumber:INumber
    { public string v;
        public long num
        {
            get;
            set;
        }
        public SimpleLongNumber (long num)
        {
            this.num = num;

        }

        public INumber add(INumber n)
        {
            SimpleLongNumber s = (SimpleLongNumber)n;
            SimpleLongNumber a = new SimpleLongNumber(num + s.num);
            return a;
            

        }

        public int CompareTo(object obj)
        {
            int k = 0;
            SimpleLongNumber s = (SimpleLongNumber)obj;
            if (num > s.num) k = 1;
            if (num < s.num) k = -1;
            if (num == s.num) k = 0;
            return k;
        }

        public INumber sub(INumber n)
        {
            SimpleLongNumber s = (SimpleLongNumber)n;
            if(num<s.num) throw new NotNaturalNumberException("Error #135: INumber must be >= N");
            SimpleLongNumber a = new SimpleLongNumber(num - s.num);

            
            return a;

        }
        public void toString()
        {
            Console.WriteLine(v);
        }
    }
}
