﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab17
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная 17");
            Console.WriteLine("Введите номер задания");
            var task = Console.ReadLine();
            switch (task)
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;

            }
        }
        static void Task1()
        {
            Console.WriteLine("Введите название школы по которой нужно совершить поиск");
            string school = Console.ReadLine();
            StreamReader Failin = new StreamReader("Input.txt ",Encoding.GetEncoding(1251)); 
            StreamWriter Failout = new StreamWriter("Output.txt ");
            int count = File.ReadAllLines("Input.txt").Length;
            int k = 0;
            Stud[] students = new Stud[count];
            while (!Failin.EndOfStream)
            {
                string s = Failin.ReadLine();
                string[] stu = s.Split(' ');

                if (stu.Length > 5)
                {
                    for (int i = 6; i < stu.Length; i++)
                    {
                        stu[5] = stu[5] + " " + stu[i];
                    }
                }

                students[k] = new Stud(stu[0], stu[1], stu[2], stu[3], stu[4], stu[5]);

                k++;


            }
            Failin.Close();
            for (int i = 0; i < students.Length - 1; i++)
            {
                if (students[i].CompareTo(students[i + 1]) == -1)
                {
                    Stud person = students[i + 1];
                    students[i + 1] = students[i];
                    students[i] = person;

                }

            }
            foreach (Stud a in students)
            {
                if (a.SchoolChek(school))
                {
                    Failout.WriteLine(a.ToString(), true);
                }
            }
            Failout.Close();

        }
        static void Task2()
        {
            StreamReader Fin1 = new StreamReader("Input1.txt", Encoding.GetEncoding(1251));
            StreamWriter Fout1 = new StreamWriter("Output1.txt");

            int  k = 0;
            int count = File.ReadAllLines("Input1.txt").Length;
            Exam[] exam = new Exam[count];
            while (!Fin1.EndOfStream)
            {
                string s = Fin1.ReadLine();
                string[] ex = s.Split(' ');

                exam[k] = new Exam(ex[0], ex[1], ex[2], Convert.ToInt32((ex[3])), ex[4], ex[5], ex[6]);
                k++;
            }
            Fin1.Close();
            for (int i = 0; i < exam.Length - 1; i++)
            {
                if (exam[i].CompareTo(exam[i + 1]) == -1)
                {
                    Exam per = exam[i + 1];
                    exam[i + 1] = exam[i];
                    exam[i] = per;


                }         
            }
            foreach (Exam a in exam)
            {
                if (a.Pass())
                {
                    Fout1.WriteLine(a.ToString(), true);
                }
            }
            Fout1.Close();
        }

    }
}
