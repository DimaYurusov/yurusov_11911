﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab17
{
    class Exam:IComparable
    {
        public string name;
        public string futname;
        public string surname;
        public int grup;
        public string exam1;
        public string exam2;
        public string exam3;

        public Exam(string name,string surname,string futname, int grup,string exam1,string exam2,string exam3)
        {
            this.surname = surname;
            this.name = name;
            this.futname = futname;
            this.grup = grup;
            this.exam1 = exam1;
            this.exam2 = exam2;
            this.exam3 = exam3;
        }
        public override string ToString()
        {
            return surname + " " + name + " " + futname + " " + grup + " " + exam1 + " " + exam2 + " " +exam3;
        }
        public int CompareTo(object obj1)
        {
            Exam s1 = (Exam)obj1;
            int a;

            if (grup >= s1.grup)
            {
                if (grup > s1.grup) a = 1;
                else a = 0;
            }
            else a = -1;
            return a;
        }
        public bool Pass()
        {
            if (exam1== "Зачет" && exam2== "Зачет" && exam3 =="Зачет") return true;
            else return false;
        }
    }

}
