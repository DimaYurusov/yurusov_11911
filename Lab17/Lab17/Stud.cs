﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab17
{
    class Stud:IComparable
    {   public string name;
        public string futname;
        public string surname;
        protected DateTime year;
        public string town;
        public string school;

        public Stud(string surname, string name,string futname, string year, string town, string school)
        {
            this.surname=surname;
            this.name = name;
            this.futname = futname;
            this.year= DateTime.Parse(year);
            this.town=town;
            this.school=school;
        }
        public override string ToString()
        {
            return surname + " " + name + " " + futname + " " + year + " " + town + " " + school;
        }
        public int CompareTo(object obj1)
        {
            int a;
            Stud s1 = (Stud)obj1;

            if (year >= s1.year)
            {
                if (year > s1.year) a = 1;
                else a = 0;
            }
            else a = -1;
            return a;
        }

        public bool SchoolChek(string s)
        {
            if (school == s) return true;
            else return false;
        }
    }
}




