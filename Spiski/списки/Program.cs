﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ASD_2
{

    class Segment : IComparable
    {//ввод данных
        public int x1 { get; set; }
        public int y1 { get; set; }
        public int x2 { get; set; }
        public int y2 { get; set; }
        public double Length()
        {
            return Math.Sqrt(Math.Pow(Math.Abs(x1 - x2), 2) + Math.Pow(Math.Abs(y1 - y2), 2));//длина
        }
        public int CompareTo(object obj)// ссортировка по длине 
        {
            obj = obj as Segment;
            if (this.Length() > ((Segment)obj).Length())
            {

                return 1;
            }
            else if (this.Length() < ((Segment)obj).Length())
            {

                return -1;
            }
            else return 0;
        }
    }
    class GraphicPic
    {
        MyList<Segment> ml = new MyList<Segment>();
        public GraphicPic Build(int[,] mas)
        {
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                ml.AddLast(new Segment() { x1 = mas[i, 0], y1 = mas[i, 1], x2 = mas[i, 2], y2 = mas[i, 3] });// распределение каждого элемента на свои позиции
            }
            return this;
        }
        public GraphicPic angleList()
        {
            GraphicPic temp = new GraphicPic();
            var el = ml.Head;
            while (el != null)
            {
                if (el.Info.x1 < el.Info.x2 && (el.Info.y2 - el.Info.y1) == (el.Info.x2 - el.Info.x1) && el.Info.y2 > el.Info.y1) // если разность между х и у одинакова 
                {
                    temp.Insert(el.Info);// элемент добавляется в список 
                }
                if (el.Info.x1 < el.Info.x2 && el.Info.y2 > el.Info.y1 && 2 * (el.Info.y2 - el.Info.y1) == Math.Pow(Math.Pow(el.Info.y2 - el.Info.y1, 2) + Math.Pow(el.Info.x2 - el.Info.x1, 2), 1 / 2))// математическая формула на проверку 30 градусов
                {
                    temp.Insert(el.Info); // элемент добавлется в список 
                }
                el = el.Next;// рассматриваем следующий элемент 
            }
            return temp;

        }
        public GraphicPic lenthList(int a, int b)
        {
            GraphicPic temp = new GraphicPic();
            var el = ml.Head;
            while (el != null)
            {
                if (el.Info.Length() >= a && el.Info.Length() <= b)// смотрим , лежит ли длина отрезка в нужном диапозоне 
                {
                    temp.Insert(el.Info);// добавляем в список 
                }
                el = el.Next;// переходим к следующему элементу 
            }
            return temp;
        }
        public void Insert(Segment f)
        {
            bool b = false;
            var el = ml.Head;
            while (el != null)
            {
                if (el.Info.x1 == f.x1 && el.Info.y1 == f.y1 && el.Info.x2 == f.x2 && el.Info.y2 == f.y2) // есть данный отрезок уже в списке
                {
                    b = true;

                }
                el = el.Next;// переходим к другому
            }
            if (!b)
            {
                ml.AddLast(f); // записываем элемент 
            }
        }
        public void sort()
        {
            List<Segment> ls = new List<Segment>();
            var el = ml.Head;
            while (el != null)
            {
                ls.Add(el.Info);
                el = el.Next;
            }

            ls.Sort();
            ml.Head = null;



            for (int i = 0; i < ls.Count; i++)
            {
                ml.AddLast(ls[i]);
            }
        }
        public override string ToString()
        {
            var sb = new StringBuilder();
            var el = ml.Head;
            while (el != null)
            {
                sb.Append($"({el.Info.x1};{el.Info.y1}) ~ ({el.Info.x2};{el.Info.y2}) ({el.Info.Length()})\n");
                el = el.Next;
            }

            return sb.ToString();
        }
    }
}