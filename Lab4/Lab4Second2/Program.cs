﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Second2
{
    class Program
    {
            static double F(double x)
            {
                var y = 0.0;
                if (Math.Abs(x) < 3)
                    y = Math.Sin(x);
                else if (Math.Abs(x) >= 9)
                    y = Math.Sqrt(x * x + 1) - Math.Sqrt(x * x + 5);
                else
                    y = Math.Sqrt(x * x + 1) / Math.Sqrt(x * x + 5);
                return y;
            }
        static void Main(string[] args)
        {
            Console.WriteLine("a=");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("b=");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("h=");
            double h = double.Parse(Console.ReadLine());
            for (double i = a; i <= b; i += h)
                Console.WriteLine("F({0:F2})={1:F4}", i, F(i));
            Console.ReadKey();
        }

    }

}
