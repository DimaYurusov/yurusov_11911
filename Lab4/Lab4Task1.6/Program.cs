﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Task1._6
{
    class Program
    {
        static int Fx(int x)
        {
            return x % 5 == 0 ? x / 5 : x + 1;
        }
        static void Main(string[] args)
        {
            Console.Write("a=");
            int a = int.Parse(Console.ReadLine());
            var x = Fx(a);
            Console.WriteLine(x);
            Console.ReadKey();
        }
    }
}
