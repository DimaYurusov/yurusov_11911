﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Second4
{
    class Program
    {
      
            static double F(double x)
            {
                var result = 0.0;
                if (Math.Abs(x) <= 0.1)
                    result = Math.Pow(x, 3) - 0.1;
                else if (Math.Abs(x) >= 0.2)
                    result = Math.Pow(x, 3) + 0.1;
                else
                    result = 0.2 + x - 0.1;
                return result;
            }
        
        static void Main(string[] args)
        {
            Console.WriteLine("a=");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("b=");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("h=");
            double h = double.Parse(Console.ReadLine());
            for (double i = a; i <= b; i += h)
                Console.WriteLine("F({0:F2})={1:F4}", i, F(i));
            Console.ReadKey();
        }
    } 
}
