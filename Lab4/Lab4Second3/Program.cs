﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Second3
{
    class Program
    {

            static double F(double x, double a)
            {
                var result = 0.0;
                if (x < a)
                    result = 0;
                else if (x > a)
                    result = (x - a) / (x + a);
                else if (x == a)
                    result = 1;
                return result;
            }
        
        static void Main(string[] args)
        {
            Console.WriteLine("a=");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("b=");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("h=");
            double h = double.Parse(Console.ReadLine());
            for (double i = a; i <= b; i += h)
                Console.WriteLine("F({0:F2})={1:F4}", i, F(i));
            Console.ReadKey();
        }

    
    }
}
