﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {

        static double min(double x, double y)
        {
            return (x < y) ? x : y;
        }

        static void Main(string[] args)
        {
            Console.Write("x=");
            double x = double.Parse(Console.ReadLine());
            Console.Write("y=");
            double y = double.Parse(Console.ReadLine());
            double z = min(3 * x, 2 * y) + min(x - y, x + y);
            Console.WriteLine("z=" + z);
            Console.ReadKey();
        }
    }
}
