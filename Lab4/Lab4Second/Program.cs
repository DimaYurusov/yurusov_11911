﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Second
{
    class Program
    {
        static double F(double x)
        {
            double y;
            if (x >= 0.9)
                y = 1 / Math.Pow((0.1 + x), 2);
            else if (x < 0)
                y = x * x + 0.2;
            else
                y = 0.2 * x + 0.1;
            return y;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("a=");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("b=");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("h=");
            double h = double.Parse(Console.ReadLine());
            for (double i = a; i <= b; i += h)
                Console.WriteLine("F({0:F2})={1:F4}", i, F(i));
            Console.ReadKey();
        }
        
    }

}
