﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Task1._5
{
    class Program
    {
        static int Fx(int x)
        {
            return (x % 2) != 0 ? 0 : x / 2;
        }
        static void Main(string[] args)
        {
            Console.Write("a=");
            int a = int.Parse(Console.ReadLine());
            var x = Fx(a);
            Console.WriteLine(x);
            Console.ReadKey();
        }
    }
}
