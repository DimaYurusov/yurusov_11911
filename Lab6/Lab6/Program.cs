﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
             {   
            class Program
        {
            static void Main(string[] args)
            {
                Console.WriteLine("Лабораторная работа 6.");
                Console.Write("Введите номер задания: ");

                var task = Console.ReadLine();

                switch (task)
                {
                    case "1":
                        Task1();
                        break;
                    case "2":
                        Task2();
                        break;
                    case "3":
                        Task3();
                        break;
                    case "4":
                        Task4();
                        break;
                    case "5":
                        Task5();
                        break;

                }
            Console.ReadKey();
            }

            static void Task1()
            {
                Random random = new Random();

                int length = 20;
                int[] weights = new int[length];

                for (int i = 0; i < weights.Length; i++)
                {
                    weights[i] = random.Next(50, 101);
                }

                Console.WriteLine("Вес 20-ти человек ( в кг):");
                PrintArray(weights);
            }

            static void Task2()
            {
                Console.Write("Введите количество элементов массива: ");
                int length = int.Parse(Console.ReadLine());
                Console.Write("Введит число А: ");
                double a = double.Parse(Console.ReadLine());

                double[] myArray = new double[length];

                Console.WriteLine("Введите элементы массива:");
                InputArray(myArray);

                Console.WriteLine();

                DoubleElementsOfArray(myArray);
                Console.WriteLine(" Элементы массива , умноженные на два");
                PrintArray(myArray);

                Console.WriteLine();

                DivideByFirstElement(myArray);
                Console.WriteLine("Элементы массива , деленные на первый элемент массива: ");
                PrintArray(myArray);

                Console.WriteLine();

                SubtractTheNumber(myArray, a);
                Console.WriteLine("Элементы массивва , уменьшенные на число А:");
                PrintArray(myArray);
            }

            static void Task3()
            {
                Console.Write("Введите количество элементов массива: ");
                int length = int.Parse(Console.ReadLine());
               Console.WriteLine("Введите элементы массива:");
                int[] myArray = new int[length];
                InputArray(myArray);

                if (IsSumOfElementsEven(myArray))
                {
                    Console.WriteLine("Сумма элементов массива - четное число.");
                }
                else
                {
                    Console.WriteLine("Сумма элементов массива - нечетное число");
                }

                if (IsSumOfSquaredElementsFiveDigitNumber(myArray))
                {
                    Console.WriteLine("Семма квадратов элемента массива - пятизначное число");
                }
                else
                {
                    Console.WriteLine("Сумма квадратов элемента массива - непятизначное число");
                }
              }

             static void Task4()
              {
                 Console.Write("Введите кол-во домов: ");
                   int a = Convert.ToInt32(Console.ReadLine());
                   int[] dom = new int[a + 1];
                  Input(dom, 1, 8);

                for (int i = 1; i <= a; i++)
                {
                   Console.WriteLine(i + " - " + dom[i] + " ");
                }
                   int s1 = 0, s2 = 0;
                  for (int i = 1; i <= a; i += 2)
                   {
                     s1 += dom[i];
                   }
                  for (int i = 2; i <= a; i += 2)
                   {
                     s2 += dom[i];
                   }
                 if (s1 > s2)
                {
                  Console.WriteLine("На стороне с нечетными номерами живет больше людей");
                }
            else if (s1 < s2)
              {
                Console.WriteLine("На стороне с нечетными номерами живет меньше людей");

              }
                else Console.WriteLine("Жителей одинаковое кол-во");
            }
           

        private static void Input(int[]a, int x, int y)
        {
            Random r = new Random();
            for (int i = 0; i < a.Length; i++)
            {

                a[i] = r.Next(x, y);
            }
        }

        static bool IsSumOfElementsEven(int[] myArray)
        {
            var sum = 0;
            for (int i = 0; i < myArray.Length; i++)
            {
                sum += myArray[i];
            }

            return sum % 2 == 0 ? true : false;
         }
        static void Task5()
        {  
               int l = q.Next(1, 10);
            int[] mas5 = new int[l + 1];
            Input(mas5, -10, 10);
            for (int i = 0; i <= l; i++)
            {
                if (mas5[i] == 0)
                {
                    mas5[i] = 1;
                }
            }
               Print(mas5);
               Console.WriteLine();
               int change = 0;
               for (int i = 1; i <= l; i++)
            {
                 if ((mas5[i] > 0 && mas5[i - 1] < 0) || (mas5[i] < 0 && mas5[i - 1] > 0))
                {
                    change += 1;
                }
            }
               Console.WriteLine($"Знак изменился {change} раз");

        }

        static void Print(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + " ");
            }
        }
  

            static bool IsSumOfSquaredElementsFiveDigitNumber(int[] myArray)
            {
                var sum = 0;

                for (int i = 0; i < myArray.Length; i++)
                {
                    sum += myArray[i] * myArray[i];
                }

                return (sum > 9999) && (sum < 100000) ? true : false;
            }

            static void DoubleElementsOfArray(double[] myArray)
            {
                for (int i = 0; i < myArray.Length; i++)
                {
                    myArray[i] *= 2;
                }
            }

            static void DivideByFirstElement(double[] myArray)
            {
                var first = myArray[0];
                for (int i = 0; i < myArray.Length; i++)
                {
                    myArray[i] /= first;
                }
            }

            static void SubtractTheNumber(double[] myArray, double a)
            {
                for (int i = 0; i < myArray.Length; i++)
                {
                    myArray[i] -= a;
                }
            }

            static void InputArray(int[] myArray)
            {
                for (int i = 0; i < myArray.Length; i++)
                {
                    myArray[i] = int.Parse(Console.ReadLine());
                }
            }

            static void InputArray(double[] myArray)
            {
                for (int i = 0; i < myArray.Length; i++)
                {
                    myArray[i] = double.Parse(Console.ReadLine());
                }
            }

            static void PrintArray(int[] myArray)
            {
                for (int i = 0; i < myArray.Length; i++)
                {
                    Console.WriteLine(myArray[i]);
                }
            }

            static void PrintArray(double[] myArray)
            {
                for (int i = 0; i < myArray.Length; i++)
                {
                    Console.WriteLine(myArray[i]);
                }
            }
        }
    }

