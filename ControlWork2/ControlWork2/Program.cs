﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ControlWork2
{
    class Program
    {
        public static object MaxValue { get; private set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Контрольная 2 Вариант 4");
            Console.WriteLine("Номер задания :");
            var task = Console.ReadLine();
            switch (task)
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;




            }
        }
        static void Task1()
        {

            Console.WriteLine("Случайно сгенерированыые числа");
            Random random = new Random();
            var count = 0;
            var sum = 0;
            while (count < 10)
            {

                var num = random.Next(0, int.Parse(MaxValue.ToString())).ToString();
                var pattern = @"[0,2,4,6,8]{3,5}";
                if (Regex.IsMatch(num, pattern))
                {
                    count++;

                    Console.WriteLine(" " + num);

                }
                sum++;

            }
          
            Console.WriteLine("Общее количество сгенерированных чисел" + sum);



        }
        static void Task2()
        {
            Console.WriteLine("введите прямоугольную матрицу");
            Console.WriteLine("Введите длину строки");
            int m = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите длину столбца");
            int n = int.Parse(Console.ReadLine());
            int count = 0;
            int min = 0;

            int[,] a = new int[m, n];
            Random random = new Random();
            for (int i = 0; i < a.GetLength(0); i++, Console.WriteLine())
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    a[i, j] = random.Next(-100, 100);

                    if (a[i, j] == 0)
                    {
                        Console.Write(a[i, j] + " ");
                    }
                }
                if (i<0)                               
                    count++;
                    

               


            }
            Console.WriteLine("Количестов элементов в тех строках , в которых хотя бы один элемент равен 0 :" + count);

            for (int i = 0; i < a.GetLength(0); i++, Console.WriteLine())
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        min = a[i, j];
                        if (a[i + 1, j + 1] < a[i, j])
                        {
                            min = a[i + 1, j + 1];
                        }


                    }


                }
            }
            Console.WriteLine("Количестов элементов в тех строках , в которых хотя бы один элемент равен 0 :" + count);
            Console.WriteLine("Минимальная сумма диагоналей" + min);
        }
    }
}

