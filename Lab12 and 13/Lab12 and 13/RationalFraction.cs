﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab12
{
    class RationalFraction
    {
        int a;
        int b;
        public RationalFraction()
        { }
        public RationalFraction(int a, int b)
        {
            this.a=a;
            this.b = b;
        }
        public void reduce()
        {
            for (int i = a; i > 1; i--)
            {
                if ((a % i == 0) && (b % i == 0))
                {
                    a = a / i;
                    b = b / i;
                }
            }

        }
        public string toString()
        {
            return "(" + this.a.ToString() + "/" + this.b.ToString() + ")";
        }
        public RationalFraction add(RationalFraction obj)
        {
            RationalFraction temp = new RationalFraction();
            temp.a = this.a * obj.b + this.b * obj.a;
            temp.b = this.b * obj.b;
            temp.reduce();
            return (temp);


        }
        public void add2(RationalFraction obj)
        {
            a = this.a * obj.b + this.b * obj.a;
            b = this.b * obj.b;
            reduce();


        }
        public RationalFraction sub(RationalFraction obj)
        {
            RationalFraction temp = new RationalFraction();
            temp.a = (this.a * obj.b) -( this.b * obj.a);
            temp.b = this.b * obj.b;
            temp.reduce();
            return (temp);


        }
        public void sub2(RationalFraction obj)
        {
            a = (this.a * obj.b) - (this.b * obj.a);
            b = this.b * obj.b;
            reduce();
        }
        public RationalFraction mult(RationalFraction obj)
        {
            RationalFraction temp = new RationalFraction();
            temp.a = this.a * obj.a;
            temp.b = this.b * obj.b;
            temp.reduce();
            return (temp);


        }
        public void mult2(RationalFraction obj)
        {
            a = this.a * obj.a;
            b = this.b * obj.b;
            reduce();


        }
        public RationalFraction div(RationalFraction obj)
        {
            RationalFraction temp = new RationalFraction();
            temp.a = this.a * obj.b;
            temp.b = this.b * obj.a;
            temp.reduce();
            return (temp);


        }
        public void div2(RationalFraction obj)
        {
            a = this.a * obj.b;
            b = this.b * obj.a;
            reduce();
        }
        public double value()
        {
            return (double)a / b;
        }
        public Boolean equals(RationalFraction obj)
        {
            if (value() > obj.value())

                return true;
            else
                return false;

        }
        public int numberPart()
        {
            return (int)a / b;
        }
    }
}
