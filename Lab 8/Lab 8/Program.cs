﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_8
{
    class Program
    {
        private static string InputString;

        public static StringBuilder InputStringBuilder { get; private set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная  №8");
            Console.Write("Введите номер задания ");
            

            var task = Console.ReadLine();

            switch (task)
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;
                case "3":
                    Task3();
                    break;
                case "4":
                    Task4();
                    break;
                case "5":
                    Task5();
                    break;
                case "6":
                    Task6();
                    break;
                case "7":
                    Task7();
                    break;
                case "8":
                    Task8();
                    break;
                case "9":
                    Task9();
                    break;
                case "10":
                    Task10();
                    break;
                    Console.ReadKey();
            }
        }

        static void Task1()
        {
            Console.WriteLine("Введите строку:");
            var s = Console.ReadLine();
            var a = new StringBuilder(s);
            for (int i = 0; i < s.Length - 1; i += 2) 
            {
                var c = a[i + 1]; 
                a[i + 1] = a[i];
                a[i] = c;
            }
            Console.WriteLine(a.ToString());
          
        }
        static void Task2()
        {
            
                Console.WriteLine("Введите строку");
                string s = Console.ReadLine();
                int a = 0;
                for (int i = 0; i < s.Length; i++)
                {
                    if (char.IsLetter(s[i]))
                        a++;

                }
               Console.WriteLine("Количество букв равно :{0}", a);              
         }
        static void Task3()
        {
            Console.WriteLine("Введите строку");
            string s = Console.ReadLine();
            for (int i = 0; i < s.Length - 1; i++)
                if (s[i] == s[i + 1])
                    Console.WriteLine("В данной строке имеются два одинаковых символа , стоящих вместе");
            else
                    Console.WriteLine("В данной строке нет двух одинаковых символов , стоящих вместе");

        }
        static void Task4()
        {
            string s;
            string result;
            Console.WriteLine(" Напишите строку ");
            s = Console.ReadLine();
            if (s.Length % 2 != 0) 
                result = s.Remove((s.Length / 2), 1); 
            else
                result = s.Remove((s.Length / 2) - 1, 2);
            Console.WriteLine("Измененная строка:",result);
            
        }
        static void Task5()
        {
            Console.WriteLine("Введите строку ");
            Console.WriteLine("подстрока substr1 ");
            Console.WriteLine("подстрока substr2 ");
            StringBuilder a = new StringBuilder(Console.ReadLine()) ;
            StringBuilder a1 = new StringBuilder(Console.ReadLine());
            StringBuilder a2 = new StringBuilder(Console.ReadLine());
            a = a.Replace(a1.ToString(), a2.ToString());
            Console.WriteLine( a.ToString());
        }

      static void Task6()
        {
            Console.WriteLine("Введите строку");
            string s = Console.ReadLine();
            int sum = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsNumber(s[i]))
                {

                    sum += Convert.ToInt32(s[i].ToString());

                }
            }
            Console.WriteLine("Сумма всех цифр в строке:{0}", sum);
        }
        static void Task7()
        {
            Console.WriteLine("Введите строку");
            string s = Console.ReadLine();
            int index = s.IndexOf(':');
            if (index >= 0)
                Console.WriteLine("Все символы до двоеточия:",s.Substring(0, index));
            else
                Console.WriteLine("Все символы до двоеточия:",s);
        }
        static void Task8()
        {
            Console.WriteLine("Введите строку");
            string s = Console.ReadLine();
            Console.WriteLine("Последовательность символов после последнего двоеточия:",s.Substring(s.LastIndexOf(':') + 1));
        }
        static void Task9()
        {
            Console.WriteLine("Введите строку с двумя запятыми" );
            string s= Convert.ToString(Console.ReadLine());
            int a = s.IndexOf(',');
            int b = -1;
            if (a >= 0)
                b = s.IndexOf(',', a);
            if (a >= 0 && b >= 0)
                s = s.Remove(a + 1, b - a - 1);
            Console.WriteLine( String.Format(s));
        }
        static void Task10()
        {
           
        }
    }
}


   

        
    
