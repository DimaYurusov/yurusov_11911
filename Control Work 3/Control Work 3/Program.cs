﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_3
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Console.WriteLine("Контрольная 3 . Вариант 3");
            Console.WriteLine("Ex1");
            Ex1();
            Console.WriteLine("Ex2");
            Ex2();



        }
        static void Ex1()
        {
            Console.WriteLine("Введите фамилию по которой нужно произвести поиск");
            string surname = Console.ReadLine();
            StreamReader fIn = new StreamReader("input.txt");
            StreamWriter Failout = new StreamWriter("Output.txt ");
            int count = 0; string word = "";
            while ((word = fIn.ReadLine()) != null) count++;

            Znak[] p = new Znak[count];

            fIn.BaseStream.Position = 0;

            string[] words = new string[3];
            int i = 0;
            while ((word = fIn.ReadLine()) != null)
            {
                words = word.Split(' ');
                p[i] = new Znak(words[0], words[1], words[2]);
                i++;
            }
            fIn.Close();
            for (int k = 0; k < i - 1; k++)
            {
                if (p[k].CompareTo(p[k + 1]) == 1)
                {
                    Znak s = p[k];
                    p[k] = p[k + 1];
                    p[k + 1] = s;

                }
            }

            foreach (Znak a in p)
            {
                if (a.SurnamelChek(surname))
                {
                    Failout.WriteLine(a.ToString(), true);
                }
            }
            Failout.Close();

        }

        static void Ex2()
        { 
             Game game = new Game();
            Gamer gamer1 = new Gamer();
            Gamer gamer2 = new Gamer();
            gamer1.input();
            gamer2.input();
            game.GameCicle(gamer1.HP, gamer1.Name, gamer2.HP, gamer2.Name);
        }
    }
}
