﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_3
{
    class Znak:IComparable
    {
        string surname { get;  set; }
        string name;
        string znak;
        public Znak(string surname, string name, string znak)
        {
            this.surname=surname;
            this.name= name;
            this.znak = znak;
        }
        public int CompareTo(object obj)
        {
            Znak p = obj as Znak;
            if (p != null)
                return this.surname.CompareTo(p.surname);
            else
                throw new Exception("Невозможно сравнить два объекта");
        }
        public bool SurnamelChek(string s)
        {
            if (surname == s) return true;
            else return false;
        }
        public override string ToString()
        {
            return (surname + " " + name + " " + znak);
        }
       




    }
}
