﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_3
{
    class Gamer
    {
            public string Name { set; get; }
            public int HP { set; get; }
            public Gamer(string name, int hp)
            {
                Name = name;
                HP = hp;
            }
            public Gamer()
            {

            }
            public void input()
            {
                Console.WriteLine("Введите имя игрока : ");
                Name = Console.ReadLine();
                Console.WriteLine("Введите запас здоровья : ");
                HP = int.Parse(Console.ReadLine());
            }


        
    }
}
