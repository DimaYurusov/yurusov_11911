﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_3
{
    class Game:Gamer
    {
        public Game()
        {
            StreamReader fIn = new StreamReader("input.txt");
            int count = 0; string word = "";
            while ((word = fIn.ReadLine()) != null) count++;

            Znak[] p = new Znak[count];

            fIn.BaseStream.Position = 0;


        }

        public void GameCicle(int hp1, string name1, int hp2, string name2)
        {
            
            Gamer Player1 = new Gamer(name1, hp1);
            Gamer Player2 = new Gamer(name2, hp2);
            while (Player1.HP > 0 || Player2.HP > 0)
            {
                Console.WriteLine("Введите урон первого игрока");
                int damage = int.Parse(Console.ReadLine());


                Player2.HP -= damage;
                Console.WriteLine($"{damage} урона нанёс первый игрок\n{Player2.HP} Осталось у второго игрока");
                if (Player2.HP <= 0) break;

                Console.WriteLine("Введите урон второго игрока");
                int damage1 = int.Parse(Console.ReadLine());

                Player1.HP -= damage1;
                Console.WriteLine($"{damage1} урона нанёс второй игрок\n{Player1.HP} Осталось у первого игрока");
                if (Player1.HP <= 0) break;
            }
            if (Player1.HP <= 0) Console.WriteLine("победил игрок номер 2"); else Console.WriteLine("победил игрок номер 1");
        }
    }
}
