﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Введите целые числа A, B, X.");
            var a = int.Parse(Console.ReadLine());
            var b = int.Parse(Console.ReadLine());
            var x = int.Parse(Console.ReadLine());

            Console.WriteLine();
            for (int i = a; i <= b; i++)
            {
                if (i % 10 == x)
                    Console.WriteLine(i);
                else continue;
            }

            Console.WriteLine();

            int j = a - 1;
            while (j <= b)
            {
                j++;
                if (j % 10 == x)
                    Console.WriteLine(j);
                else continue;
            }

            Console.WriteLine();

            int c = a - 1;
            do
            {
                c++;
                if (c % 10 == x)
                    Console.WriteLine(c);
                else continue;
            }
            while (c <= b);
            Console.ReadKey();

        }
    }
}
