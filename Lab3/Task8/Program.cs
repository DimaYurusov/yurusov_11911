﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int a = 10; a < 100; a++)
            {
                if (Math.Abs(a % 10 - a / 10) <= 1)
                    Console.WriteLine(a);
                else continue;
            }

            Console.WriteLine();

            int b = 9;
            while (b < 100)
            {
                b++;
                if (Math.Abs(b % 10 - b / 10) <= 1)
                    Console.WriteLine(b);
                else continue;
            }

            Console.WriteLine();

            int c = 9;
            do
            {
                c++;
                if (Math.Abs(c % 10 - c / 10) <= 1)
                    Console.WriteLine(c);
                else continue;
            }
            while (c < 100);
            Console.ReadKey();
        }
    }
}
