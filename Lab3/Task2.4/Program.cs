﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2._4
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 41;
            while (a <= 80)
            {
                Console.Write(a + " ");
                if (a % 10 == 0)
                    Console.WriteLine();
                a += 1;
            }
            Console.ReadKey();
        }
    }
}
