﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 60;
            while (a >= 10)
            {
                Console.WriteLine(a);
                a -= 2;
            }

            Console.WriteLine();

            int b = 60;
            do
            {
                Console.WriteLine(b);
                b -= 2;
            }
            while (b >= 10);

            Console.WriteLine();

            for (int c = 60; c >= 10; c -= 2)
                Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
