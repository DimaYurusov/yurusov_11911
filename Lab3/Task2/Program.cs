﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int grammsInFoot = 453;
            for (int a = 1; a <= 10; a++)
                Console.WriteLine("{0} фунтов = {1}г", a, a * grammsInFoot);

            Console.WriteLine();

            int b = 1;
            while (b <= 10)
            {
                Console.WriteLine("{0} фунтов = {1}г", b, b * grammsInFoot);
                b++;
            }

            Console.WriteLine();
            int c = 1;

            do
            {
                Console.WriteLine("{0} фунтов = {1}г", c, c * grammsInFoot);
                c++;
            }
            while (c <= 10);
            Console.ReadKey();
        }
    }
}
