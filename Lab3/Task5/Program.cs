﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите целые числа A и B.");
            var a = int.Parse(Console.ReadLine());
            var b = int.Parse(Console.ReadLine());

            Console.WriteLine();
            for (int i = a + 1; i <= b; i++)
            {
                if (i > 0)
                    Console.WriteLine(i);
                else continue;
            }

            Console.WriteLine();

            int j = a ;
            while (j <= b - 1)
            {
                j++;
                if (j > 0)
                    Console.WriteLine(j);
                else continue;
            }

            Console.WriteLine();

            int c = a ;
            do
            {
                c++;
                if (c > 0)
                    Console.WriteLine(c);
                else continue;
            }
            while (c <= b - 1);
            Console.ReadKey();

        }
    }
}
