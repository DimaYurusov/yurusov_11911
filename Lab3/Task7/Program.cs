﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int a = 10; a < 100; a++)
            {
                if (a % 10 != a / 10)
                    Console.WriteLine(a);
                else continue;
            }

            Console.WriteLine();

            int b = 9;
            while (b < 99)
            {
                b++;
                if (b % 10 != b / 10)
                    Console.WriteLine(b);
                else continue;
            }

            Console.WriteLine();

            int c = 9;
            do
            {
                c++;
                if (c % 10 != c / 10)
                    Console.WriteLine(c);
                else continue;
            }
            while (c < 99);
            Console.ReadKey();
        }
    }
}
