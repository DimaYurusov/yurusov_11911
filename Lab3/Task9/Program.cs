﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int a = 100; a < 1000; a++)
            {
                int firstD = a / 100;
                int secondD = (a / 10) % 10;
                int thirdD = a % 10;
                if ((firstD == secondD) || (firstD == thirdD) || (secondD == thirdD))
                    Console.WriteLine(a);
                else continue;
            }

            Console.WriteLine();

            int b = 99;
            while (b < 999)
            {
                b++;
                int firstD = b / 100;
                int secondD = (b / 10) % 10;
                int thirdD = b % 10;
                if ((firstD == secondD) || (firstD == thirdD) || (secondD == thirdD))
                    Console.WriteLine(b);
                else continue;
            }

            Console.WriteLine();

            int c = 99;
            do
            {
                c++;
                int firstD = c / 100;
                int secondD = (c / 10) % 10;
                int thirdD = c % 10;
                if ((firstD == secondD) || (firstD == thirdD) || (secondD == thirdD))
                    Console.WriteLine(c);
                else continue;
            }
            while (c < 999);
            Console.ReadKey();

        }
    }
}
