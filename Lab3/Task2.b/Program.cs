﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.b
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            for (int i = 1; i <= a; i++, Console.WriteLine())
            {
                for (int j = 1; j <= i; j++)
                    Console.Write(j + " ");

            }
            Console.ReadKey();
        }
    }
}
