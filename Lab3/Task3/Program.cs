﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите целые числа A и B.");
            var a = int.Parse(Console.ReadLine());
            var b = int.Parse(Console.ReadLine());

            Console.WriteLine();

            for (int i = b; i >= a; i--)
                Console.WriteLine(i * i * i);

            Console.WriteLine();

            int j = b;
            while (j >= a)
            {
                Console.WriteLine(j * j * j);
                j--;
            }

            Console.WriteLine();

            int c = b;
            do
            {
                Console.WriteLine(c * c * c);
                c--;
            }
            while (c >= a);
            Console.ReadKey();
        }
    }
}
