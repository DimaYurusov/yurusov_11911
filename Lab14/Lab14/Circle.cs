﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Circle:Figure
       
    {
        public Circle(double x)
        {
            this.x = x;
           



        }
        public override double Perimeter()
        {
            return x*Math.PI*2;
        }
        public override double Square()
        {
            return Math.PI*Math.Pow(x,2);
        }
        public override void Print()
        {
            Console.WriteLine(" Круг с радиусом  " + x );
            Console.WriteLine("периметр = " + Perimeter()+ "  " + "Площадь = " + Square());
        }
    }
}
