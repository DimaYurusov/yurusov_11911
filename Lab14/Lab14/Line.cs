﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Line:Function
    {
        public double a;
        public double b;
        public double x;

        public Line(double a, double b,double x)
        {
            this.a = a;
            this.b = b;
            this.x = x;
        }

        public override double Draw()
        {
            return a * x + b;
        }
        public override void Print1()
        {
            Console.WriteLine(" Функции Line при х={0} имеет значение {1} ", x, Draw());
        }


    }
}

