﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Triangle: Figure
    {
        public Triangle( double x, double y , double z) 
        {
            this.x = x;
            this.y = y;
            this.z = z;
            

        }
        public override double Perimeter()
        {
            return x + z + y;
        }
        public override double Square()
        {
            return Math.Sqrt((x + y + z) / 2 * (((x + y + z) / 2) - x) * (((x + y + z) / 2) - y) * (((x + y + z) / 2) - z));
        }
        public override void Print()
        {
            Console.WriteLine(" Треугольник со строронами {0} ,{1},{2}",x,y,z);
            Console.WriteLine("периметр = " + Perimeter() + "  " + "Площадь = " + Square());
        }
    }
}
