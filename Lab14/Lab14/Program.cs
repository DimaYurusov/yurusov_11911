﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная  №14" +
                "");
            Console.Write("Введите номер задания ");


            var task = Console.ReadLine();

            switch (task)
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;
                case "3":
                    Task3();
                    break;

            }
        }
        static void Task1()
        {

            Figure[] fi = new Figure[6];
            fi[0] = new Rectangle(3.0, 4.0);
            fi[1] = new Triangle(4.0, 5.0, 2.0);
            fi[2] = new Triangle(3.0, 4.0, 5.0);
            fi[3] = new Rectangle(3.0, 2);
            fi[4] = new Triangle(3.0, 5.0, 8.0);
            fi[5] = new Circle(3.0);

            for (int i = 0; i < fi.Length; i++)
            {
                fi[i].Print();

            }
        }
            static void Task2()
        {
            Console.WriteLine("Введите занчение х:");
            int x = int.Parse(Console.ReadLine());
            Function[] func = new Function[6];
            func[0] = new Line(1 ,5,x);
            func[1] = new Kub(4,6,1,x);
            func[2] = new Hyperbola(3,4,x);
            func[3] = new Kub(6, 1, 2, x);
            func[4] = new Line(4, 2, x);           
            func[5] = new Hyperbola(1, 2, x);

            for (int i = 0; i < func.Length; i++)
            {
                func[i].Print1();

            }


        }
        static void Task3()
        {
            Edition[] izd = new Edition[6];
            izd[0] = new eleBook("451 градус по фаренгейту"," Бредбери","azbyka.ry", " это классика научной фантастики, ставшая классикой мирового кинематографа в воплощении знаменитого французского режиссера Франсуа Трюффо");
            izd[1] = new Book("Мастер и маргарита", "Булгаков", 1930, "экспо");
            izd[2] = new Article("Как прожить","Иванов","Известие",75,1992);
            izd[3] = new Article("Россия впереди всех","Григорьев","Комерсант",785,2014 );
            izd[4] = new Book("война и мир","Толстой",1812,"эксмо");
            izd[5] = new eleBook("Тайнственный остров остров","Верн","litmir.me", "Самыми страшными войнами являются гражданские, когда ненависть зашкаливает все пределы, когда брат идет против брата, когда попасть в плен может быть страшнее смерти. Именно в такой ситуации оказалась пятерка северян, чье стремление к свободе было сильнее страха погибнуть в плену или при попытке к бегству. Но результат бегства на воздушном шаре оказался непредсказуемым: в результате урагана их занесло на необитаемый остров. Множество приключений и опасностей поджидают их там, но отвага, знания и изобретательность позволят им не только уцелеть, но и обустроить свой быт. А остров, только казавшийся необитаемым, на самом деле давно имеет своего Хозяина, могущественного, сильного и справедливого, помогающего островитянам в случае смертельной опасности, но желающего скрыть свою личность от всех");

            Console.WriteLine("Введите фамилию автора, по которому нужно выполнить поиск ");

            string sur = Console.ReadLine();
            for (int i = 0; i < 6; i++)
            {
                if (sur[i].autorEdition(sur) == true)
                {
                    sur[i].print();
                }
            }


        }

    }
    
}
