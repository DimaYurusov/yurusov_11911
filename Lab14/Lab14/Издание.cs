﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    abstract class Edition
    {
        public string Name { protected get; set; }
        public string Autor { protected get; set; }
        public Edition(string name, string autor)
        {
            Name= name;
            Autor = autor;
        }
        public abstract void print();
        public abstract bool autorEdition(string sur);

    }
}
