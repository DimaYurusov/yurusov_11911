﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Rectangle: Figure
    {
        public Rectangle(double x, double y)
        {
            this.x = x;
            this.y = y;
            


        }
        public override double Perimeter()
        {
            return (x + y)*2;
        }
        public override double Square()
        {
            return x * y;
        }
        public override void Print()
        {
            Console.WriteLine(" Прямоугольник со сторонамиронами {0} и {1}" , x, y);
            Console.WriteLine("периметр = " + Perimeter()+ "  " + "Площадь = " + Square());
        }
    }
}
