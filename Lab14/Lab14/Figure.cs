﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
   abstract class Figure
    {    
        public abstract double Perimeter();
       
        public abstract double Square();

        abstract public void Print();
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }

     
    }
    


}
