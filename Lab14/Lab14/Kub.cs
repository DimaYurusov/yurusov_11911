﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Kub:Function
    {
        public double a;
        public double b;
        public double c;
        public double x;

        public Kub(double a, double b, double c, double x)
        {
            this.a = a;
            this.b = b;
            this.b = c;
            this.x = x;
        }

        public override double Draw()
        {
            return a * x * x + b * x + c;
        }
        public override void Print1()
        {
            Console.WriteLine(" Функции Hyperbola при х={0} имеет значение {1} ", x, Draw());
        }
    }
}
