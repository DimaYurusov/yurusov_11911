﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Book : Edition
    {
        int year;
        public string Edition { private get; set; }
        public Book(string name, string autor, int year, string edition) : base(name, autor)
        {
            this.year = year;
            Edition = edition;
        }
        public override void print()
        {
            Console.WriteLine(@"Книга {0}. Автор : {1}. {2}-го года издания. Издательство {3}", Name, Autor, year, Edition);
        }
        public override bool autorEdition(string sur)
        {
            if (sur == Autor) return true;
            else return false;
        }
    }
}
