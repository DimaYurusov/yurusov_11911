﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Article : Edition
    {
        string magazinename;
        int number;
        int year;
        public Article(string name, string autor, string magazinename, int number, int year) : base(name, autor)
        {
            this.magazinename = magazinename;
            this.number = number;
            this.year = year;
        }
        public override void print()
        {
            Console.WriteLine("Статья {0}-№{4}. Автор : {1}. Название журнала {2}.  {3}-го года издания.", Name, Autor, magazinename, year, number);

        }
        public override bool autorEdition(string sur)
        {
            if (sur == Autor)
                return true;
            else return false;
        }
    }
}
