﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{

    class eleBook : Edition
    {
        string link;
        string annotation;
        public eleBook(string name, string autor, string link, string annotation) : base(name, autor)
        {
            this.link = link;
            this.annotation = annotation;
        }
        public override void print()
        {
            Console.WriteLine("Электронная книга {0}. Автор : {1}. Ссылка на ресурс {2}. Аннотация: {3}.", Name, Autor, link, annotation);
        }
        public override bool autorEdition(string sur)
        {
            if (sur == Autor) return true;
            else return false;
        }
    }
}
