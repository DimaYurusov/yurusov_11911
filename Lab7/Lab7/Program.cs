﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Program
    {
        static Random random = new Random();
        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная  №7");
            Console.Write("Введите номер задания ");

            var task = Console.ReadLine();

            switch (task)
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;
                case "3":
                    Task3();
                    break;
                case "4":
                    Task4();
                    break;
                case "5":
                    Task5();
                    break;
                case "6":
                    Task6();
                    break;
                    Console.ReadKey();   
            }
        }
        static void Task1()
        {


            Console.WriteLine("Введите длину строки:");            
            Console.WriteLine("Введите длину столбца:");
            int n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            int[,] arr = new int[m, n];
            int min = 0; int max = 10;
            Input(arr, n, m, min, max);
            Out(arr, n, m);
            TaskA(arr, n, m);
            TaskB(arr, n, m);
            TaskA1(arr, n, m);
        }
        static void Task2()
        {
            Console.WriteLine("Введите длину строки :");            
            Console.WriteLine("Введите длину столбца :");
            int n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            int[,] arr2 = new int[m, n];
            Console.WriteLine("Введите минимальное значение ");
           int min = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите максимальное значение ");
            int max = int.Parse(Console.ReadLine());
            Input(arr2, n, m, min, max);
            Out(arr2, n, m);
            Swap(arr2, n, m);
            Out(arr2, n, m);
        }
        static void Task3()
        {

           var m = 18;
           var n = 36;
            int[,] arr3 = new int[m, n];
           int min = 0;
           int max = 2;
            Input(arr3, n, m, min, max);
            Out(arr3, n, m);
            Check(arr3, n, m);
        }

        static void Task4()
        {
            Console.WriteLine("Введите длину строки :");
            Console.WriteLine("Введите длину столбца :");
            int n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите минимальное значение ");
           int  min = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите максимальное значение ");
           int  max = int.Parse(Console.ReadLine());
            int[,] arr4 = new int[m, n];
            Input(arr4, n, m, min, max);
            Out(arr4, n, m);
            CheckSum1(arr4, n, m);
            CheckSum2(arr4, n, m);
        }
        static void Task5()  
        {

            Console.WriteLine("Введите длину строки :");
            Console.WriteLine("Введите длину столбца :");
            int n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите минимальное значение ");
           int min = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите максимальное значение");
           int max = int.Parse(Console.ReadLine());
            int[,] arr5 = new int[m, n];
            Input(arr5, n, m, min, max);
            Out(arr5, n, m);
            SumDiag(arr5, n, m);
            ChetAntiDiagCheck(arr5, n, m);
        }

        static void Task6()
        {

            Console.WriteLine("Введите длину строки :");
           int  n = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите длину столбца :");
           int  m = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите минимальное значение ");
           int min = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите максимальное значение ");
            int max = int.Parse(Console.ReadLine());
            int[,] arr6 = new int[m, n];
            Input(arr6, n, m, min, max);
            Out(arr6, n, m);
            int[] stroke = new int[m];
            StrokeA(arr6, n, m, stroke);
            StrokeB(arr6, n, m, stroke);
        }
        static void Out(int[,] a, int b, int c)
        {
            for (int i = 0; i < c; i++)
            {
                for (int j = 0; j < b; j++)              
                {
                    Console.Write($"{ a[i, j]} ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        static void TaskA(int[,] a, int n, int m)
        {
            Console.WriteLine("введите номер строки ");
            int n1 = int.Parse(Console.ReadLine());
           int k = n1 -1;
            for (int j = 0; j < n; j++)               
            {
                Console.Write($"{ a[k, j]} ");
            }
            Console.WriteLine();
        }
        static void TaskB(int[,] a, int n, int m)
        {
            Console.WriteLine("введите номер столбца ");     
            int m1 = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int q = m1 - 1;
            for (int j = 0; j < m; j++)
            {
                Console.WriteLine($"{ a[j, q]} ");
            }
        }
        static void TaskA1(int[,] a, int n, int m)
        {
            int b = 0;
            if ((m - 1) >= 2)
            {
                for (int j = 0; j < n; j++)                
                {
                    b += a[2, j];
                }
                Console.WriteLine($"сумма элементов третьей строки {b}");
            }
            else
                Console.WriteLine("третьей строки не существует");
        }
        static void Swap(int[,] a, int n, int m)
        {
            for (int i = 0; i < (m/2); i++)
            {
                for (int j = 0; j < n; j++)                 
                {
                    int q = a[i, j];
                    a[i, j] = a[(m - 1) - i, j];
                    a[(m - 1) - i, j] = q;
                }
            }
          
        }

        static void Input(int[,] a, int n, int m, int min, int max)
        {
            Random rnd = new Random();
            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)               
                {
                    a[i, j] = rnd.Next(min, max);
                }
        }
        static void Check(int[,] a, int n, int m)
        {
            Console.WriteLine("Введите номер вагона ");
            int m1 = int.Parse(Console.ReadLine());
            m1 -= 1;
            int k1 = 0;                          
            for (int j = 0; j < n; j++)
            {
                if (a[m1, j] != 1) k1 += 1;
            }
            Console.WriteLine($"В {m1 + 1} вагоне {k1} свободных мест");
           
        }
        static void StrokeA(int[,] a, int n, int m, int[] stroke)
        {

            for (int i = 0; i < n; i++)             
            {
                for (int j = 0; j < m; j++)
                    if (a[j, i] % 2 == 0 && a[j, i] > 0)
                        stroke[i] += a[j, i];
                Console.Write($"{stroke[i]} ");
            }

        }
        static void StrokeB(int[,] a, int n, int m, int[] line)
        {

            for (int i = 0; i < m; i++)            
            {
                int max = 0;
                for (int j = 0; j < n; j++)
                {
                    max = Math.Max(Math.Abs(a[i, j]), max);
                    line[i] = max;
                }
                Console.Write($"{line[i]} ");
            }

        }
        static void CheckSum2(int[,] a, int n, int m)
        {
            int k = 0;                      
            for (int i = 0; i < m; i++)
                k += a[i, 0];
            int max = k;
            for (int i = 1; i < n; i++)
            {
                int k1 = 0;
                for (int j = 0; j < m; j++)
                {
                    k1 += a[j, i];
                }
                max = Math.Max(max, k1);
            }
            Console.WriteLine($"максимальная сумма столбца: {max}");

        }
        static void CheckSum1(int[,] a, int n, int m)
        {
            int k = 0;                        
            for (int j = 0; j < n; j++)
                k += a[0, j];
            int min = k;
            for (int i = 1; i < m; i++)
            {
                int k1 = 0;
                for (int j = 0; j < n; j++)
                {
                    k1 += a[i, j];
                }
                min = Math.Min(min, k1);
            }
            Console.WriteLine($"минимальная сумма строки: {min}");
            Console.WriteLine();
        }
        static void ChetAntiDiagCheck(int[,] a, int n, int m)
        {
            int k = 0;
            for (int j = 0; j < n; j++)               
                if (a[j, (n - 1) - j] % 2 == 0)
                    k++;

            Console.WriteLine($"количество чёт. элементов побочной диагонали {k}");
        }

        static void SumDiag(int[,] a, int n, int m)
        {
            int k = 0;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)          
                    if (i == j)
                        k += a[i, j];
            Console.WriteLine($"Сумма главной диагонали равна {k}");
            Console.WriteLine();
        }

       
     
    }
}
