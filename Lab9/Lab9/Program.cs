﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9
{
    class Program
    {
        private static readonly int j;

        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная  №9");
            Console.Write("Введите номер задания ");


            var task = Console.ReadLine();

            switch (task)
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;
                case "3":
                    Task3();
                    break;
                case "4":
                    Task4();
                    break;
                case "5":
                    Task5();
                    break;
                case "6":
                    Task6();
                    break;
                case "7":
                    Task7();
                    break;
                case "8":
                    Task8();
                    break;
                case "9":
                    Task9();
                    break;
                    Console.ReadKey();
            }
        }
        static void Task1()
        {
            Console.WriteLine("Введите предложение ");
            string s = Console.ReadLine();
            string [] a  = s.Split(' ', '.', ',', '?', '!', ':', ';');
            Console.WriteLine("число N ");
            int n = int.Parse(Console.ReadLine());
            foreach (string st in a)
                if (st.Length <= n)
                    Console.WriteLine("Все слова ,в которых количство букв меньше числа N {0}",st);
                         
        }
        static void Task2()
        {
            Console.WriteLine("Введите предложение ");
            string s = Console.ReadLine();
            string[] a = s.Split(' ', '.', ',', '?', '!', ':', ';');
            foreach (string word in a)
                if (word[0] == word.ToUpper()[0])
                    Console.WriteLine("Все слова , которые начинаются с заглавной буквы {0}",word);
           
        }
        static void Task3()
        {
            Console.WriteLine("Введите предложение:");
            string  s =Console.ReadLine();
            string[] sentens = s.ToLower().Split(new[] { ' ', '.', ',', '?', '!', ':', ';' });
            string[] a=sentens;
            string Result="";
            for (int i = 0; i < sentens.Length; i++)
                for (int j = 0; j < sentens.Length; j++)
                    if (sentens[i] == sentens[j] && i != j)
                    {
                        a[i] = "";
                        a[j] = "";
                    }
            for (int i=0;i<sentens.Length-1;i++)
            {
                if (a[i]!="") Result += a[i]+" ";
            }
            Console.WriteLine(Result.ToString());
            
        }
        static void Task4()
        {
            Console.WriteLine("Введите предложение");
            string a = Console.ReadLine();

            Console.WriteLine("Введите слово , которое нужно подсчитать:");
            string word = Console.ReadLine();
            int count = 0;
            string[] n = a.Split(' ', '.', ',', '?', '!', ':', ';');
            foreach (string s in n)
            {
                if (s == word) count++;
            }

            Console.WriteLine(" Слово {0} встречается в тексте {1} раз", word, count);

        }
            
        static void Task5()
        {
            Console.WriteLine("Введите предложение");
            string s = Console.ReadLine();
            int max = 0;
            string  a = "";
            string[] words = s.Split(new[] { ' ', '.', ',', '?', '!', ':', ';' },
                StringSplitOptions.RemoveEmptyEntries);
            foreach (string word in words)
                if (word.Length > max)
                {
                    max = word.Length;
                    a = word;
                }
            Console.WriteLine("Самое длиное слово в строке: {0}", a);
        }
            static void Task6()
        {
            Console.WriteLine("Введите предложение: ");
            string[] s = Console.ReadLine().Split(' ', '.', ',', '?', '!', ':', ';');
            
            for (int i = 0; i < s.Length; i++)
            {
                int sum = 0;
                for (int j = 0; j < s.Length; j++)
                    if
                        (s[i] == s[j])
                    { 
                        sum++;
                    }
                if  (sum == 1)
                    Console.WriteLine ("Слова которые употребляются только один раз :{0}",s[i] + " ");
            }
        }
        static void Task7()
        {
            Console.WriteLine("Введите предложение: ");
            string[] s = Console.ReadLine().Split(' ', '.', ',', '?', '!', ':', ';');
            Console.WriteLine("Введите число N:");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < s.Length; i++)
            {
                double sum = 0;
                for (int j = 0; j < s.Length; j++)
                    if
                        (s[i] == s[j])
                    {
                        sum++;
                    }
                if (sum > n)
                    Console.WriteLine("Слова которые употребляются больше n раз :{0}", s[i] );
            }
        }
        static void Task8()
        {
            Console.WriteLine("Введите предложение: ");
            string[] s = Console.ReadLine().Split(' ', '.', ',', '?', '!', ':', ';');
            Array.Sort(s);
            for (int i = 0; i < s.Length; i++)
                Console.WriteLine("Слова в алфавитном порядке:{0}",s[i]);

        }
        static void Task9()
        {
            Console.WriteLine("Введите предложение: ");
            string k;
            string[] s = Console.ReadLine().Split(' ', '.', ',', '?', '!', ':', ';');
            for (int i = 0; i < s.Length; i++)
                for (int j = i + 1; j < s.Length; j++)
                    if (s[i].Length > s[j].Length)
                    {
                        k = s[i];
                        s[i] = s[j];
                        s[j] = k;
                    }
            for (int i = 0; i < s.Length; i++)
                if (!string.IsNullOrEmpty(s[i]))
                    Console.WriteLine("Слова в порядке возростания их длин :{0}",s[i] );
           
        }
     
    }
}
