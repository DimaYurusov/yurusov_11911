﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab12_1_2_13_3_4_
{
    class Person
    {
        public string Name { protected set; get; }
        public string Surname { protected set; get; }
        public DateTime Bd { protected set; get; }
        int age;
        char gender ;
        public char Gender
        {

            get
            {

                return gender;
            }
            set
            {
                if ((value == 'М') || (value == 'Ж') || (value == 'м') || (value == 'ж')) 
                {
                    this.gender = value;
                }
                else
                    Console.WriteLine("Проверьте корректность вводимого пола (М) и (Ж)");


            }

        }
        public Person()
        {
            Name = "";
            Surname = "";
            Bd = DateTime.MinValue;

        }
        public Person(string name, string lastname, string age, char gender)
        {
            this.Name = name;
            this.Surname = lastname;
            this.Bd = DateTime.Parse(age);
            this.gender = gender;
        }
      
        public virtual void write()
        {
            Console.WriteLine("Имя - " + Name);
            Console.WriteLine("Фамилия - " + Surname);
            Console.WriteLine("Возраст - " + Age());
            Console.WriteLine("Пол - " + gender);

        }


        public int Age()
        {
            DateTime datanow = DateTime.Today;
            int YearAge = datanow.Year - Bd.Year;
            if (datanow.Month > Bd.Month || (datanow.Month == Bd.Month && datanow.Day >= Bd.Day))
                YearAge--;
            return YearAge;
        }

    }
}


