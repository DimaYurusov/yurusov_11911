﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab12_1_2_13_3_4_
{
    class PersonsHours: Persons
    {
        public int Hours { get; private set; }
        public int HoursSalary { get; private set; }
        public PersonsHours(string name, string lastname, string age, char gender, int money, int prize, int time, int hours) : base(name, lastname, age, gender, money, prize, time)
        {
            Hours = hours;
            HoursSalary = money;
            money = Hours * money;
        }
        public PersonsHours() 
        {
            Hours = 0;
            HoursSalary = 0;
        }
      

        }
      


    }
    

