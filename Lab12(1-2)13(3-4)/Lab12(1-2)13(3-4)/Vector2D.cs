﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab12_1_2_13_3_4
{
    class Vector2D
    {
    double x;
    double y;
    public Vector2D() { }
    public Vector2D(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
    public Vector2D add(Vector2D obj)
    {
        Vector2D temp = new Vector2D();
        temp.x = this.x + obj.x;
        temp.y = this.x + obj.y;
        return temp;
    }
    public void add2(Vector2D obj)
    {
        x = this.x + obj.x;
        y = this.x + obj.y;

    }
    public Vector2D sub(Vector2D obj)
    {
        Vector2D temp = new Vector2D();
        temp.x = this.x - obj.x;
        temp.y = this.x - obj.y;
        return temp;
    }
    public void sub2(Vector2D obj)
    {
        x = this.x - obj.x;
        y = this.x - obj.y;

    }
    public Vector2D mult(double a)
    {
        Vector2D temp = new Vector2D();
        temp.x = this.x * a;
        temp.y = this.y * a;
        return temp;

    }
    public void mult2(double b)
    {
        x = this.x * b;
        y = this.x * b;

    }
    public string toString()
    {
        return "(" + this.x.ToString() + "," + this.y.ToString() + ")";
    }
    public double length()
    {
        return Math.Sqrt(x * x + y * y);
    }
    public double scalarProduct(Vector2D obj)
    {
        return x * obj.x + y * obj.y;

    }
    public double cos(Vector2D obj)
    {
        double cos = scalarProduct(obj) / (length() * obj.length());
        return cos;

    }
    public Boolean equals(Vector2D obj)
    {
        if (length() < obj.length())
            return true;
        else
            return false;

    }

}
}
