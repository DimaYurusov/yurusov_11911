﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab12_1_2_13_3_4_
{
   

        class Persons : Person
        {
            public int money { protected set; get; }
            public int prize { protected set; get; }
            private int time;
            public int Time
            {
                private set
                {

                    if ((value < 0))
                    {
                        Console.WriteLine("Не корректный стаж работы");

                    }
                    else
                        time = value;

                }
                get { return time; }
            }
            public Persons(string name, string lastname, string age, char gender, int money, int p, int time) : base(name, lastname, age, gender)
            {
                this.money = money;
                this.prize = prize;
                this.Time = time;
            }
            public Persons() 
            {
                money = 0;
                prize = 0;
                time = 0;
            }

            public override void write()
            {
                base.write();
                Console.WriteLine("Зарплата " + money);
                Console.WriteLine("{0}% премии ", prize);
                Console.WriteLine("Стаж работы " + Time);
            }
            public virtual double totalMoney()
            {
                return ((1 + ((double)prize / 100)) * money);
            }
            public double nalog()
            {
                return (totalMoney() * 0.13);
            }
            public double salary()
            {
                return totalMoney() - nalog();
            }
            public void totalPrem()
            {
            if (Time > 10)
            {
                prize = prize * 2;
            }
            }

        }




       
    }

