﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWork
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите 10 чисел больше нуля");
            int k, kl = 0;
            for (int a =0; a!=10; a++)
            {
                k = int.Parse(Console.ReadLine());
                if (IsSquare(k)) kl++;
                    }
            Console.WriteLine("У вас среди 10-ти чесиел {0} являются квадратными", kl);
            Console.WriteLine();

            Console.WriteLine("Введите координаты x и y");
            int x = int.Parse(Console.ReadLine());
            int y = int.Parse(Console.ReadLine());
            Quarter(x, y);
            Console.ReadKey();

        }
        public static bool IsSquare (int k)
        {
            bool a;
            int z = Convert.ToInt32(Math.Sqrt(k));
            if (Math.Pow(z, 2) == k) a = true;
            else a = false;
            return a;
                                 
        }

        private static void Quarter(int x, int y)
        {
            if (x>0)
            {
                if (y > 0)
                    Console.WriteLine("Точка лежит в первой четверти");
                else if (y < 0)
                    Console.WriteLine("Точка лежит в четвертой четверти");
             
            }
            else
            {
                if (y > 0)
                    Console.WriteLine("Точка лежит во второй четверти");
                else if (y < 0)
                    Console.WriteLine("Точка лежит в третьей четверти");
            }
     
        }
    }
}
