﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab18
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная  №18 , первая часть ");
          

            Console.WriteLine("ex1.2");
            Ex1_2();
            Console.WriteLine("ex1.3");
            Ex1_3();
            Console.WriteLine("ex1.4");
            Ex1_4();
            Console.WriteLine("ex1.5");
            Ex1_5();
            Console.WriteLine("ex1.6");
            Ex1_6();
            Console.WriteLine("ex1.7");
            Ex1_7();
            Console.WriteLine("ex1.8");
            Ex1_8();
            Console.WriteLine("ex1.9");
            Ex1_9();
            Console.WriteLine("ex1.10");
            Ex1_10();
            Console.WriteLine("ex1.11");
            Ex1_11();
            Console.WriteLine("ex1.12");
            Ex1_12();
            Console.WriteLine("ex1.13");
            Ex1_13();
            Console.WriteLine("ex1.14");
            Ex1_14();
            Console.WriteLine("ex1.15");
            Ex1_15();
        }

        static void Ex1_2()
        {

            Console.WriteLine("Enter a:"); double a = double.Parse(Console.ReadLine());
            
            Matrix m = new Matrix(a);
            Console.WriteLine("Result:");
            m.Print();

        }

        static void Ex1_3()
        {
            
            Console.WriteLine("Enter a :"); double a = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter b :"); double b = double.Parse(Console.ReadLine());
            Matrix m = new Matrix(a, b);
            Console.WriteLine("Result:");
            m.Print();
        }

        static void Ex1_4()
        {
            Matrix m1 = new Matrix(1, 10);
            Matrix m2 = new Matrix(1, 10);
            Console.WriteLine("Matrix 1:");
           
            m1.Print();
            Console.WriteLine("Matrix 2:");
            
            m2.Print();
            Console.WriteLine("Result:");
            Matrix sum = m1.Add(m2);           
            sum.Print();
        }

        static void Ex1_5()
        {
            Matrix m1 = new Matrix(1, 10);
            Matrix m2 = new Matrix(1, 10);
            Console.WriteLine("Matrix 1:");
            m1.Print();
            Console.WriteLine("Matrix 2:");
            m2.Print();
            Console.WriteLine("Result:");
            m1.Add2(m2);
            m1.Print();
        }

        static void Ex1_6()
        {
            Matrix m1 = new Matrix(1, 10);
            Matrix m2 = new Matrix(1, 10);
            Console.WriteLine("Matrix 1:");
            m1.Print();
            Console.WriteLine("Matrix 2:");
            m2.Print();
            Console.WriteLine("Result:");
            Matrix sub = m1.Sub(m2);
            sub.Print();
        }

        static void Ex1_7()
        {
            Matrix m1 = new Matrix(1, 10);
            Matrix m2 = new Matrix(1, 10);
            Console.WriteLine("Matrix 1:");
            m1.Print();
            Console.WriteLine("Matrix 2:");
            m2.Print();
            Console.WriteLine("Result:");
            m1.Sub2(m2);
            m1.Print();
        }

        static void Ex1_8()
        {
            Matrix m1 = new Matrix(1, 10);
            Console.WriteLine("Matrix:");
            m1.Print();
            Console.WriteLine("Enter double:"); double b = double.Parse(Console.ReadLine());
            Console.WriteLine("Result:");
            Matrix mult = m1.MultNumber(b);
            mult.Print();
        }

        static void Ex1_9()
        {
            Matrix m1 = new Matrix(1, 10);
            Console.WriteLine("Matrix:");
            m1.Print();
            Console.WriteLine("Enter number:"); double b = double.Parse(Console.ReadLine());
            Console.WriteLine("Result:");
            m1.MultNumber2(b);
            m1.Print();
        }

        static void Ex1_10()
        {
            Matrix m1 = new Matrix(1, 10);
            Matrix m2 = new Matrix(1, 10);
            Console.WriteLine("Matrix 1:");
            m1.Print();
            Console.WriteLine("Matrix 2:");
            m2.Print();
            Console.WriteLine("Result:");
            Matrix m = m1.Mult(m2);
            m.Print();
        }

        static void Ex1_11()
        {
            Matrix m1 = new Matrix(1, 10);
            Matrix m2 = new Matrix(1, 10);
            Console.WriteLine("Matrix 1:");
            m1.Print();
            Console.WriteLine("Matrix 2:");
            m2.Print();
            Console.WriteLine("Result:");
            m1.Mult2(m2);
            m1.Print();
        }

        static void Ex1_12()
        {
            Matrix m1 = new Matrix(1, 10);
            Console.WriteLine("Matrix:");
            m1.Print();
            Console.WriteLine("Result:");
            double s = m1.Det();
            Console.WriteLine(s);
        }

        static void Ex1_13()
        {
            Matrix m1 = new Matrix(1, 10);
            m1.Print();
            Console.WriteLine("Result:");
            m1.Transpon();
            m1.Print();
        }

        static void Ex1_14()
        {
            Matrix m1 = new Matrix(1, 10);
            Console.WriteLine("Matrix:");
            m1.Print();
            Console.WriteLine("Result:");
            Matrix m = m1.InverseMatrix();
            m.Print();
        }

        static void Ex1_15()
        {
            Matrix m1 = new Matrix(1, 10);
            Console.WriteLine("Matrix:");
            m1.Print();
            Console.WriteLine("Result:");
            Matrix m = m1.EquivalentDiagonal();
            m.Print();
        }


    }
    }

