﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab18
{
    class Matrix
    {
        
        public double[,] a = new double[2, 2];


        public Matrix()
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    a[i, j] = 0;
                }
            }

        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           



            public Matrix(double a)
        {
            for (int i = 0;i<2;i++)
            {
                for (int j = 0;j<2;j++)
                {
                    this.a[i, j] = a;
                }
            }



        }
        public Matrix(double min,double max)
        {

            Random r = new Random();
            double[,] mat = new double[2,2];
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    mat[i, j] = r.NextDouble()*(max-min)+min;
        }
        public Matrix Add(Matrix obj)
        {
            double ma1 = a[0, 0] + obj.a[0, 0];
            double ma2 = a[0, 1] + obj.a[0, 1];
            double ma3 = a[1, 0] + obj.a[1, 0];
            double ma4 = a[1, 1] + obj.a[1, 1];
            Matrix mat = new Matrix();
            mat.a[0, 0] = Math.Round(ma1, 3);
            mat.a[0, 1] = Math.Round(ma2, 3);
            mat.a[1, 0] = Math.Round(ma3, 3);
            mat.a[1, 1] = Math.Round(ma4, 3);
            return mat;

        }
        public void  Add2(Matrix obj)
        {
            a[0, 0] += obj.a[0, 0];
            a[0, 0] = Math.Round(a[0, 0], 3);
            a[0, 1] += obj.a[0, 1];
            a[0, 0] = Math.Round(a[0, 1], 3);
            a[1, 0] += obj.a[1, 0];
            a[1, 0] = Math.Round(a[1, 0], 3);
            a[1, 1] += obj.a[1, 1];
            a[1, 1] = Math.Round(a[1, 1], 3);
        }
        public Matrix Sub(Matrix obj)
        {
            double ma1 = a[0, 0] - obj.a[0, 0];
            double ma2 = a[0, 1] - obj.a[0, 1];
            double ma3 = a[1, 0] - obj.a[1, 0];
            double ma4 = a[1, 1] - obj.a[1, 1];
            Matrix mat1 = new Matrix();
            mat1.a[0, 0] = Math.Round(ma1, 3);
            mat1.a[0, 1] = Math.Round(ma2, 3);
            mat1.a[1, 0] = Math.Round(ma3, 3);
            mat1.a[1, 1] = Math.Round(ma4, 3);
            return mat1;
        }
        public void  Sub2(Matrix obj)
        {
            a[0, 0] -= obj.a[0, 0];
            a[0, 0] = Math.Round(a[0, 0], 3);
            a[0, 1] -= obj.a[0, 1];
            a[0, 0] = Math.Round(a[0, 1], 3);
            a[1, 0] -= obj.a[1, 0];
            a[1, 0] = Math.Round(a[1, 0], 3);
            a[1, 1] -= obj.a[1, 1];
            a[1, 1] = Math.Round(a[1, 1], 3);
        }
        public Matrix MultNumber(double b)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    a[i, j] = Math.Round(a[i, j] * b, 3);
                }
            }
                Matrix mat1 = new Matrix();
                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                    mat1.a[i, j] = a[i, j];
                    }
                }
            return mat1;
        }
        public void MultNumber2(double b)
        {
            a[0, 0] *= b;
            a[0, 0] = Math.Round(a[0, 0], 3);
            a[0, 1] *= b;
            a[0, 1] = Math.Round(a[0, 1], 3);
            a[1, 0] *= b;
            a[1, 0] = Math.Round(a[1, 0], 3);
            a[1, 1] *= b;
            a[1, 1] = Math.Round(a[1, 1], 3);
        }
        public Matrix Mult(Matrix obj)
        {
            double[,] m = new double[2, 2];
            for (int i = 0;i<2;i++)
            {
                for (int j=0;j<2;j++)
                {
                    for (int b =0;b<2;b++)
                    {
                        m[i, j] += a[i, b] * obj.a[b, j];

                    }
                }
            }
            Matrix mat2 = new Matrix();
            for ( int i = 0;i<2;i++)
            {
                for ( int j= 0;j<2;j++)
                {
                    mat2.a[i, j] = Math.Round(mat2.a[i, j] = m[i, j], 2);
                }
            }
            return mat2;

        }
        public void Mult2(Matrix obj)
        {
            double[,] m = new double[2, 2];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int b = 0; b < 2; b++)
                    {
                        m[i, j] += a[i, b] * obj.a[b, j];

                    }
                }
            }
          
            a[0, 0] = m[0, 0];
            a[0, 0] = Math.Round(a[0, 0], 3);
            a[0, 1] = m[0, 1];
            a[0, 1] = Math.Round(a[0, 1], 3);
            a[1, 0] = m[1, 0];
            a[1, 0] = Math.Round(a[1, 0], 3);
            a[1, 1] = m[1, 1];
            a[1, 1] = Math.Round(a[1, 1], 3);
           
        }
        public double Det()
        {
            double s =   a[0,0]*a[1,1]-(a[0,1]*a[1,0]);
            s = Math.Round(s, 3);
            return s;
        }
        public void Transpon()
        {
            for (int i=0;i<2;i++)
            {
                for (int j = 0;j<i;j++)
                {
                    double mat = a[i, j];
                    a[i, j] = a[j, i];
                    a[j, i] = mat;
                }
            }
        }
         public Matrix InverseMatrix()
        {
            Transpon();
            a[0, 1] = -a[0, 1];
            a[1, 0] = -a[1, 0];
            Matrix matin = new Matrix();
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < i; j++)
                {
                 matin.a[i,j]= Math.Round(   matin.a[i, j] = a[i, j] * Det(),3);
                }
            }
            return matin;


         }
        public Matrix EquivalentDiagonal()
        {
            Matrix mate = new Matrix();
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (i == j)
                    {
                        mate.a[i, j] = a[i, j];
                        mate.a[i, j] = Math.Round(mate.a[i, j], 3);
                    }
                    else
                    {
                        mate.a[i, j] = 0;
                    }
                    }

                }
            return mate;
            }
        public void Print()
        {
            Matrix mat = new Matrix();
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Console.Write(mat.a[i, j] + " ");
                }
                Console.WriteLine();
            }
        }



    }


        
}
