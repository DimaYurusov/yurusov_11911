﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5TaskI
{
    class Program
    {
        static void Input(out string a, out double b, out int c, out double d)
        {
            Console.WriteLine("Введите наименование, цену, количество, скидку");
            a = Console.ReadLine();
            b = double.Parse(Console.ReadLine());
            c = int.Parse(Console.ReadLine());
            d = double.Parse(Console.ReadLine());

        }
        static void Output(string a, double b, int c, double d)
        {
            Console.WriteLine($"Имя: {a}");
            Console.WriteLine($"Цена: {b}");
            Console.WriteLine($"Количество: {c}");
            Console.WriteLine($"Скидка: {d}%");
        }
        static double CalCulate(double b, int c, double d)
        {
            double b1;
            b1 = (b - b * (d / 100)) * c;
            return b1;
        }
        static void Min(double e, double f, double b)
        {
            double b1 = (Math.Min(b, Math.Min(e, f)));
            Console.WriteLine(b1);
        }
        static void Main(string[] args)
        {
            string a; double b; int c; double d; double bl1; double bl2; double bl3;
            Input(out a, out b, out c, out d);
            Output(a, b, c, d);
            bl1 = CalCulate(b, c, d);
            Console.WriteLine(bl1);
            Input(out a, out b, out c, out d);
            Output(a, b, c, d);
            bl2 = CalCulate(b, c, d);
            Console.WriteLine(bl2);
            a = "Boeing 777";
            b = 2;
            c = 1000000;
            d = 0;
            Output(a, b, c, d);
            bl3 = CalCulate(b, c, d);
            Console.WriteLine(bl3);
            Min(bl1, bl2, bl3);
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
