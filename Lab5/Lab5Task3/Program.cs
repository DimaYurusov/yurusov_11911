﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Вdедите шестизначное число");
            F(out _);
            Console.WriteLine();
            Console.WriteLine("Введите шесть однозначных чисел");
            double b = double.Parse(Console.ReadLine());
            double c = double.Parse(Console.ReadLine());
            double d = double.Parse(Console.ReadLine());
            double e = double.Parse(Console.ReadLine());
            double f = double.Parse(Console.ReadLine());
            double g = double.Parse(Console.ReadLine());
            F(b, c, d, e, f, g);
            Console.WriteLine();
            Console.WriteLine("Введите два трехзначных числа");
            int h = int.Parse(Console.ReadLine());
            int i = int.Parse(Console.ReadLine());
            F(h, i);
            Console.ReadKey();
        }
        static void F( out int a)
        {
            a = int.Parse(Console.ReadLine());
            int a1; int a2;
            a1 = a / 1000; a2 = a % 1000;
            if (((a1 % 10) + (a1 / 10 % 10) + (a1 / 100)) == ((a2 % 10) + (a2 / 10 % 10) + (a2 / 100)))
                Console.WriteLine("Билет счастливый");
            else
                Console.WriteLine("Билет не счастливый");

        }
        static void F(double b, double c, double d, double e, double f, double g)
        {
            if (b + c + d == e + f + g) Console.WriteLine("Билет счастливый");
            else Console.WriteLine("Билет не счастливый");
        }
        static void F(int h, int i)
        {
            if (((h % 10) + (h / 10 % 10) + (h / 100)) == ((i % 10) + (i / 10 % 10) + (i / 100)))
                Console.WriteLine("Билет счастливый");
            else
                Console.WriteLine("Билет не счастливый");

        }
    }
}
