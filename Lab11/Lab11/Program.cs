﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab11
{
    class Program
    {
        private static long matches;
        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная  №11");
            Console.Write("Введите номер задания: ");


            var task = Console.ReadLine();

            switch (task)
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;
                case "3":
                    Task3();
                    break;
                case "4":
                    Task4();
                    break;
                case "5":
                    Task5();
                    break;
            }
        }
        static void Task1()
        {
            string Pattern1 = @"\d{2}-\d{2}-\d{2}";
            string Pattern2 = @"\d{3}-\d{3}";
            string Pattern3 = @"\d{3}-\d{2}-\d{2}";
            {
                Console.WriteLine("Введите строку");
                string s = Console.ReadLine();
                Match match1 = Regex.Match(s, Pattern1);
                Match match2 = Regex.Match(s, Pattern2);
                Match match3 = Regex.Match(s, Pattern3);
                if (Regex.IsMatch(s, Pattern1, RegexOptions.IgnoreCase))
                {
                    Console.WriteLine("Номер 1 :{0}", match1);
                }
                if (Regex.IsMatch(s, Pattern2, RegexOptions.IgnoreCase))
                {
                    Console.WriteLine("Номер 2 :{0}", match2);
                }
                if (Regex.IsMatch(s, Pattern3, RegexOptions.IgnoreCase))
                {
                    Console.WriteLine("Номер 3 :{0} ", match3);
                }


            }
        }
        static void Task2()
        {
            Console.WriteLine("Введиет дату в формате дд.мм.гггг");
            string s = Console.ReadLine();
            Console.WriteLine("Введите год");
            int year = int.Parse(Console.ReadLine());
            string pattern = @"\b(([1-2]?[0-9])|(3[0-1]|(0[1-9])))\.((0[1-9])|(1[0-2]))\." + year + @"\b";
            foreach (Match data in Regex.Matches(s, pattern))
            {
                if ((year > 2010) | (year < 1900))
                {
                    Console.WriteLine("Не существует");
                }
                else
                {
                    Console.WriteLine("Даты в текущем году {0} ", data);
                }

            }


        }
        static void Task3()
        {
            Console.WriteLine("Введите IP адрес");
            string s = Console.ReadLine();


            int d = -1;

            while (d < 1 || d > 9)
            {
                Console.Write("Введите цифру от 1 до 9: ");
                d = Convert.ToInt32(Console.ReadLine());
            }

            string endPattern = "";
            if (d == 1)
                endPattern = @"(1[0-9]{2}|1[0-9]|1)";
            else if (d == 2)
                endPattern = @"(25[0-5]|2[0-4][0-9]|2[0-9]|2)";
            else
                endPattern = d.ToString();

            string pattern = @"(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])\." + endPattern;
            Regex newReg = new Regex(pattern);
            string newText = newReg.Replace(s, "");
            Console.WriteLine("Новый текст:{0} ", newText);

            Console.ReadLine();
        }
        static void Task4()
        {
            Console.WriteLine("Введите IP адрес");
            string s = Console.ReadLine();

            string pattern = @"((https?|ftp)\:\/\/)?([a-z0-9]{1})((\.[a-z0-9-])|([a-z0-9-]))*\.([a-z]{2,6})(\/?)";
            foreach (Match web in Regex.Matches(s, pattern))

            {
                Console.WriteLine("Значение найденного объекта {0}", web.Value);
            }
        }
        static void Task5()
        {
            Console.WriteLine("Введиет дату в формате дд.мм.гггг , где год диапазон годов будет лежать в пределе от 1900 до 2010");
            string s = Console.ReadLine();

            string pattern = @"\b(([1-2]?[0-9])|(3[0-1]|(0[1-9])))\.((1[0-2])|(0[1-9]))\.((19[0-9][0-9])|(20[0][0-9])|(20[1][0]))\b"; 

            Regex reg = new Regex(pattern);
                    MatchCollection  matches = reg.Matches(s);
                   
                    for (int i = 0; i < matches.Count; i++)
                    {
                        string updDate = DateTime.Parse(matches[i].Value).AddDays(+1).ToShortDateString();
                        s = s.Replace(matches[i].Value, updDate);
                    }
                    Console.WriteLine("следующий день {0}",  s);
                
            }

        }

    }


